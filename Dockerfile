FROM node:18.19.1-alpine3.19
RUN apk update && apk add npm
ENV APP_ROOT /web
ENV NODE_ENV production

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}



RUN npm ci
RUN npm run build

CMD ["npm", "run", "dev"]
