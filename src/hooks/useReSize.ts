import { useState, useEffect } from "react";

export const useReSize = (): number => {
  const [width, setWidth] = useState(window.innerWidth ? window.innerWidth : 0);

  useEffect(() => {
    const handleResize = (event: any) => {
      setWidth(event.target.innerWidth);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return width;
};
