import { FC } from "react";
import styles from "./Button.module.scss";
import { IButton } from "./Button-interface";

const Button: FC<IButton> = ({ children, onClick }) => {
	return <div className={styles.button} onClick={onClick}>{children}</div>;
};
export default Button;
